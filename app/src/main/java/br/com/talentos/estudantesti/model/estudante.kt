package br.com.talentos.estudantesti.model

class Estudante {

    var nome = ""
    var areaTi = ""
    var email = ""

    override fun toString(): String {
        return "Estudante(nome='$nome', areaTi='$areaTi', email='$email')"
    }

}