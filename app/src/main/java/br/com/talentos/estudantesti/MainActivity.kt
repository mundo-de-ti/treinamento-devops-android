package br.com.talentos.estudantesti

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
//import android.support.design.widget.Snackbar
//import android.support.v7.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import br.com.talentos.estudantesti.PesquisaEstudantesActivity
import br.com.talentos.estudantesti.http.HttpHelper
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buttonAbrirCadastroEstudante = findViewById<Button>(R.id.button_cadastro)

        val buttonAbrirListaEstudante = findViewById<Button>(R.id.button_lista_estudantes)

//        val buttonPesquisa=findViewById<Button>(R.id.button_pesquisa_estudantes)

        buttonAbrirCadastroEstudante.setOnClickListener{
            val abrirCadastroEstudante = Intent (this, CadastroEstudanteActivity::class.java)
            startActivity(abrirCadastroEstudante)
        }

        buttonAbrirListaEstudante.setOnClickListener{
            val abrirListaEstudante = Intent (this, ExibeEstudanteActivity::class.java)
            startActivity(abrirListaEstudante)
        }

//        buttonPesquisa.setOnClickListener {
//            val abrirPesquisaEstudantes = Intent(this, PesquisaEstudantesActivity::class.java)
//            startActivity(abrirPesquisaEstudantes)
//        }
    }
}
