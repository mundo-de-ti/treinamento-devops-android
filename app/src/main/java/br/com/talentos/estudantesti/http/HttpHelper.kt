package br.com.talentos.estudantesti.http

import br.com.talentos.estudantesti.BuildConfig
import br.com.talentos.estudantesti.model.Estudante
import com.google.gson.Gson
import okhttp3.*
import java.io.IOException
import java.io.InputStream
import java.util.concurrent.TimeUnit


public class HttpHelper {

    val gson = Gson()

    val headerHttp=MediaType.parse("application/json; charset=utf-8")

    val client=OkHttpClient()




    fun post(json: String): String {
        val username= BuildConfig.USER_API
        val pass= BuildConfig.PASS_API
        val credential = Credentials.basic(username, pass)
        val URL_API= BuildConfig.URL_API

        val body=RequestBody
            .create(headerHttp, json)

        var request=Request.Builder()
            .url(URL_API)
            .addHeader("Authorization", credential)
            .post(body)
            .build()

        var response=client.newCall(request)
            .execute()

        return response
            .body()
            .toString()
    }

    fun get2() {
        val URL_API=(BuildConfig.URL_API + "lista")
        val request = Request.Builder().url(URL_API).get().build()
        val response=client.newCall(request).execute()
        val responseBody = response.body()
//        val pessoa = gson.fromJson( "{\"nome\":\"+nome+\"\"email\":\"+email+\"\"areaTi\":\"+areaTi+\"}",Estudante::class.java)

        if (responseBody != null){
            val resultado = responseBody.string()
            println("RESULTADO ===========" + resultado)
//            println("PESSOA ===========" + pessoa)

        }
    }
    fun get(url: String): InputStream {
        val URL_API=(BuildConfig.URL_API + "lista")
//        + "?numero=${cpfDoUsuarioLogado()}", header)
        val request = Request.Builder().url(URL_API).build()
        val response = OkHttpClient().newCall(request).execute()
        val body = response.body()
        return body!!.byteStream()
    }
}
