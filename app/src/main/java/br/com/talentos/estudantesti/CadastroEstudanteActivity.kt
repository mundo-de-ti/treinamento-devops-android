package br.com.talentos.estudantesti

import android.content.Intent
import android.os.Bundle
//import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import br.com.talentos.estudantesti.http.HttpHelper
import br.com.talentos.estudantesti.model.Estudante
import com.google.gson.Gson
import org.jetbrains.anko.doAsync
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class CadastroEstudanteActivity : AppCompatActivity() {

    lateinit var listTextAreasTi: TextView
    lateinit var listSpinnerAreasTi: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastro_estudante)

        listTextAreasTi = findViewById(R.id.text_lista_areasTi)
        listSpinnerAreasTi =this.findViewById(R.id.spinner_lista_areasTi)
        var listaAreasTi = arrayOf("Desenvolvimento", "Teste e Qualidade", "DevOps", "UX/UI Designer")
        listSpinnerAreasTi.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, listaAreasTi)

        listSpinnerAreasTi.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                listTextAreasTi.text = listaAreasTi[position]
            }
        }

        val buttonSalvar = findViewById<Button>(R.id.button_salvar)
        val editTextNome = findViewById<EditText>(R.id.edit_text_nome)
//        val editTextAreaTi = findViewById<EditText>(R.id.edit_text_areaTi)
        val editTextEmail = findViewById<EditText>(R.id.edit_text_email)

        buttonSalvar.setOnClickListener {
            val estudante = Estudante()
            estudante.nome = editTextNome.text.toString()
//            estudante.areaTi = editTextAreaTi.text.toString()
            estudante.areaTi = listTextAreasTi.text.toString()
            estudante.email = editTextEmail.text.toString()

            val gson = Gson()
            val estudanteJson = gson.toJson(estudante)

            Toast.makeText(this.applicationContext, editTextNome.text.toString() + ", perfil cadastrado para " + listTextAreasTi.text.toString(), Toast.LENGTH_LONG).show()


            doAsync {
                val http = HttpHelper ()
                http.post(estudanteJson)
            }
            val abrirListaEstudante=Intent(this,ExibeEstudanteActivity::class.java)
            startActivity(abrirListaEstudante)
        }
    }

}
