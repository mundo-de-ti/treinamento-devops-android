package br.com.talentos.estudantesti

import android.content.Intent
import android.os.Bundle
//import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import br.com.talentos.estudantesti.http.HttpHelper
import br.com.talentos.estudantesti.model.Estudante
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.Request
import org.jetbrains.anko.doAsync
import java.io.InputStream


class PesquisaEstudantesActivity : AppCompatActivity() {

    lateinit var listTextAreasTi: TextView
    lateinit var listSpinnerAreasTi: Spinner

        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pesquisa_estudantes)

        listTextAreasTi = findViewById(R.id.text_lista_areasTi)
        listSpinnerAreasTi = findViewById(R.id.spinner_lista_areasTi)
        var listaAreasTi = arrayOf("Desenvolvimento", "Teste e Qualidade", "DevOps", "UX/UI Designer")
        listSpinnerAreasTi.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, listaAreasTi)

        listSpinnerAreasTi.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?,view: View?,position: Int,id: Long) {
                listTextAreasTi.text = listaAreasTi[position]
            }
        }


        val buttonInicio=findViewById<Button>(R.id.button_voltar_inicio)
        buttonInicio.setOnClickListener {
            val voltarInicio=Intent(this, MainActivity::class.java)
            startActivity(voltarInicio)

            val buttonPesquisa=findViewById<Button>(R.id.button_pesquisa_estudantes)
            buttonPesquisa.setOnClickListener {
                val buttonPesquisa=Intent(this,PesquisaEstudantesActivity::class.java)
                startActivity(buttonPesquisa)

                doAsync {
                    val http=HttpHelper()
                    http.get2()
                }
            }
        }
    }
}