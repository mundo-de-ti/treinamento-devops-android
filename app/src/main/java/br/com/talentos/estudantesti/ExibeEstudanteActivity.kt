package br.com.talentos.estudantesti

import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
//import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import br.com.talentos.estudantesti.http.HttpHelper
import kotlinx.android.synthetic.main.activity_exibe_estudante.*
import org.jetbrains.anko.doAsync
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader

class ExibeEstudanteActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_exibe_estudante)

        GetJsonWithOkHttpClient(this.text).execute();

//        val estudante = Estudante()
//        estudante.nome = editTextNome.text.toString()
//            estudante.areaTi = editTextAreaTi.text.toString()
//        estudante.areaTi = listTextAreasTi.text.toString()
//        estudante.email = editTextEmail.text.toString()
//        val gson = Gson()
//        val estudanteJson = gson.toJson(estudante)

//        doAsync {
//            val http=HttpHelper()
//            http.get2()
//        }
//        val buttonInicio=findViewById<Button>(R.id.button_voltar_inicio)
//        buttonInicio.setOnClickListener {
//            val voltarInicio=Intent(this, MainActivity::class.java)
//            startActivity(voltarInicio)
//        }
    }

    open class GetJsonWithOkHttpClient(textView: TextView) : AsyncTask<Unit,Unit,String>() {

        private val mInnerTextView = textView

        @Deprecated("Deprecated in Java")
        override fun doInBackground(vararg params: Unit?): String? {
            val httpClient = HttpHelper()
            val stream = BufferedInputStream(
                httpClient.get(URL_API))
            return readStream(stream)
        }

        @Deprecated("Deprecated in Java")
//        @Deprecated("Deprecated in Java")
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            mInnerTextView.text = result

        }

        fun readStream(inputStream: BufferedInputStream): String {
            val bufferedReader = BufferedReader(InputStreamReader(inputStream))
            val stringBuilder = StringBuilder()
            bufferedReader.forEachLine { stringBuilder.append(it) }
            return stringBuilder.toString()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_settings) {
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        @JvmField
        val URL_API=(BuildConfig.URL_API + "lista")
    }

}